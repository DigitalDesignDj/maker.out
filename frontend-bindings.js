var $         = require('jquery-browserify'),
	ko        = require('knockout'),
	viewModel = require('./viewModel.js');

// Frontend display bindings

// This formats the credits message
viewModel.credits_message = ko.dependentObservable( function(){
	return "Credits: " + this.credits();
}, viewModel);

// Card display bindings
// These keep the proper CSS Class strings applied to each card
viewModel.card1_class = ko.dependentObservable( function(){
	var cardClass = this.card1_suit() + ' ' + this.card1_value();
	if( this.card1_hold() ){
		cardClass += ' hold';
	}
	return cardClass;
}, viewModel );

viewModel.card2_class = ko.dependentObservable( function(){
	var cardClass = this.card2_suit() + ' ' + this.card2_value();
	if( this.card2_hold() ){
		cardClass += ' hold';
	}
	return cardClass;
}, viewModel );

viewModel.card3_class = ko.dependentObservable( function(){
	var cardClass = this.card3_suit() + ' ' + this.card3_value();
	if( this.card3_hold() ){
		cardClass += ' hold';
	}
	return cardClass;
}, viewModel );

viewModel.card4_class = ko.dependentObservable( function(){
	var cardClass = this.card4_suit() + ' ' + this.card4_value();
	if( this.card4_hold() ){
		cardClass += ' hold';
	}
	return cardClass;
}, viewModel );

viewModel.card5_class = ko.dependentObservable( function(){
	var cardClass = this.card5_suit() + ' ' + this.card5_value();
	if( this.card5_hold() ){
		cardClass += ' hold';
	}
	return cardClass;
}, viewModel );
